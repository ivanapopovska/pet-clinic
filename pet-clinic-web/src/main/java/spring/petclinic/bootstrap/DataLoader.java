package spring.petclinic.bootstrap;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import spring.petclinic.domain.*;
import spring.petclinic.services.*;

import java.time.LocalDate;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;
    private final SpecialtyService specialtyService;
    private final VisitService visitService;

    public DataLoader(OwnerService ownerService, VetService vetService, PetTypeService petTypeService, SpecialtyService specialtyService, VisitService visitService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
        this.specialtyService = specialtyService;
        this.visitService = visitService;
    }

    @Override
    public void run(String... args) throws Exception {
        if(petTypeService.findAll().size() == 0){
            loadData();
        }
    }

    private void loadData() {
        PetType dog = new PetType();
        dog.setPetName("dog");
        PetType savedDogPetType = petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setPetName("cat");
        PetType savedCatPetType = petTypeService.save(cat);

        Specialty radiology = new Specialty();
        radiology.setDescription("radiology");
        Specialty savedRadiology = specialtyService.save(radiology);
        Specialty surgery = new Specialty();
        surgery.setDescription("surgery");
        Specialty savedSurgery = specialtyService.save(surgery);
        Specialty dentist = new Specialty();
        dentist.setDescription("dentist");
        Specialty savedDentist = specialtyService.save(dentist);
        Owner owner1 = new Owner();
        owner1.setFirstName("Ivana");
        owner1.setLastName("Popovska");
        owner1.setAddress("Adresa1");
        owner1.setCity("Skopje");
        owner1.setTelephone("01233456");


        Pet petIvana = new Pet();
        petIvana.setPetType(savedDogPetType);
        petIvana.setOwner(owner1);
        petIvana.setName("Kuce");
        petIvana.setBirthday(LocalDate.now());
        owner1.getPets().add(petIvana);
        ownerService.save(owner1);

        Owner owner2 = new Owner();
        ;
        owner2.setFirstName("John");
        owner2.setLastName("Thompson");
        owner2.setAddress("Adresa 2");
        owner2.setCity("Skopje");
        owner2.setTelephone("903792");


        Pet petJohn = new Pet();
        petJohn.setPetType(savedCatPetType);
        petJohn.setOwner(owner2);
        petJohn.setName("mace");
        petJohn.setBirthday(LocalDate.now());
        owner2.getPets().add(petJohn);
        ownerService.save(owner2);
        System.out.println("Loads owners");

        Visit catVisit = new Visit();
        catVisit.setPet(petIvana);
        catVisit.setDate(LocalDate.now());
        catVisit.setDescription("DESC");
        visitService.save(catVisit);

        Vet vet1 = new Vet();
        vet1.setId(1l);
        vet1.setFirstName("Vet1");
        vet1.setLastName("Vet 1 lastname");
        vet1.getSpecialities().add(savedRadiology);
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setId(2l);
        vet2.setFirstName("Vet2");
        vet2.setLastName("Vet 2 lastname");
        vet2.getSpecialities().add(savedSurgery);
        vetService.save(vet2);
        System.out.println("Loads vets");
    }
}
