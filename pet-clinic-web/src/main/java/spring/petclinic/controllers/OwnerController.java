package spring.petclinic.controllers;

import org.dom4j.rule.Mode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import spring.petclinic.domain.Owner;
import spring.petclinic.services.OwnerService;

import java.util.List;

@Controller
@RequestMapping("/owners")
public class OwnerController {

    private final OwnerService  ownerService;

    public OwnerController(OwnerService ownerService) {
        this.ownerService = ownerService;
    }


    @InitBinder
    public void SetAllowedFields(WebDataBinder webDataBinder){
        webDataBinder.setDisallowedFields("id");
    }

    @RequestMapping("/find")
    public String findOwners(Model model){
        Owner owner = new Owner();
        model.addAttribute("owner", owner);
        return "owners/findOwners";
    }
     @GetMapping
     public String processFindOwner(Owner owner, BindingResult result, Model model){
        if(owner.getLastName() == null){
            owner.setLastName("");
        }
         List<Owner> foundOwners = ownerService.findAllByLastNameLike("%"+owner.getLastName()+"%");
        if(foundOwners.isEmpty()){
            result.rejectValue("lastName", "notFound", "not found");
            return "owners/findOwners";
        }else if(foundOwners.size()==1){
            owner = foundOwners.iterator().next();
            return "redirect:/owners/" + owner.getId();
        }else{
            model.addAttribute("selections", foundOwners);
            return "owners/ownersList";
        }

     }

    @GetMapping
    @RequestMapping("/{ownerId}")
    public ModelAndView showOwner(@PathVariable Long ownerId){
        ModelAndView modelAndView = new ModelAndView("owners/ownerDetails");
        modelAndView.addObject(ownerService.findById(ownerId));
        return modelAndView;
        //model.addAttribute("owner", ownerService.findById(ownerId));
        //return "owners/ownerDetails";
    }

    @GetMapping("/new")
    public String addNewOwner(Model model){
        Owner owner = new Owner();
        model.addAttribute(owner);
        return "owners/createOrUpdateOwnerForm";
    }

    @PostMapping("/new")
    public String processNewOwner(Owner owner){
        Owner saved = ownerService.save(owner);
        return "redirect:/owners/" + saved.getId();
    }
    @GetMapping("/{ownerId}/edit")
    public String editOwner(@PathVariable Long ownerId, Model model){
        model.addAttribute("owner", ownerService.findById(ownerId));
        return "owners/createOrUpdateOwnerForm";
    }
    @PostMapping("/{ownerId}/edit")
    public String processEditOwner(Owner owner, @PathVariable Long ownerId){
        owner.setId(ownerId);
        Owner saved = ownerService.save(owner);
        return "redirect:/owners/" + saved.getId();
    }
}
