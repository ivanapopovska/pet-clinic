package spring.petclinic.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name ="types")
@AllArgsConstructor
@NoArgsConstructor
public class PetType extends BaseEntity{


    @Builder
    public PetType(Long id, String petName){
        super(id);
        this.petName = petName;
    }
    private String petName;

    public String getPetName() {
        return petName;
    }

    public void setPetName(String petName) {
        this.petName = petName;
    }

    @Override
    public String toString() {
        return petName;
    }
}
