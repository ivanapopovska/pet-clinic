package spring.petclinic.services;

import spring.petclinic.domain.Pet;

import java.util.Set;

public interface PetService extends CrudService<Pet, Long>{
}
