package spring.petclinic.services;

import org.springframework.data.repository.CrudRepository;
import spring.petclinic.domain.PetType;

public interface PetTypeService extends CrudService<PetType, Long> {
}
