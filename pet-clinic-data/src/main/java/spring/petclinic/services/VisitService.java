package spring.petclinic.services;

import spring.petclinic.domain.Visit;

public interface VisitService extends CrudService<Visit, Long> {
}
