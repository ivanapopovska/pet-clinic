package spring.petclinic.services;

import spring.petclinic.domain.Specialty;

public interface SpecialtyService extends CrudService<Specialty, Long> {
}
