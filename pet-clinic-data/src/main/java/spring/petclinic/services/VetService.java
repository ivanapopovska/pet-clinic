package spring.petclinic.services;

import spring.petclinic.domain.Owner;
import spring.petclinic.domain.Vet;

import java.util.Set;

public interface VetService extends CrudService<Vet, Long>{
}
