package spring.petclinic.services.SpringDataJpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import spring.petclinic.domain.Pet;
import spring.petclinic.repositories.PetRepo;
import spring.petclinic.services.PetService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class PetJPAService implements PetService {

    private final PetRepo petRepo;

    public PetJPAService(PetRepo petRepo) {
        this.petRepo = petRepo;
    }

    @Override
    public Set<Pet> findAll() {
        Set<Pet> pets = new HashSet<>();
        petRepo.findAll().forEach(pets::add);
        return pets;
    }

    @Override
    public Pet findById(Long aLong) {
        Optional<Pet> pet = petRepo.findById(aLong);
        return pet.orElse(null);
    }

    @Override
    public Pet save(Pet object) {
        return petRepo.save(object);
    }

    @Override
    public void deleteById(Long aLong) {
        petRepo.deleteById(aLong);
    }

    @Override
    public void delete(Pet object) {
        petRepo.delete(object);
    }
}
