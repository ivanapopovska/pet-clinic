package spring.petclinic.services.SpringDataJpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import spring.petclinic.domain.Specialty;
import spring.petclinic.repositories.SpecialtyRepo;
import spring.petclinic.services.SpecialtyService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;

@Service
@Profile("springdatajpa")
public class SpecialtyJPAService implements SpecialtyService {

    private final SpecialtyRepo specialtyRepo;

    public SpecialtyJPAService(SpecialtyRepo specialtyRepo) {
        this.specialtyRepo = specialtyRepo;
    }

    @Override
    public Set<Specialty> findAll() {
        Set<Specialty> specialties = new HashSet<>();
        specialtyRepo.findAll().forEach(specialties::add);
        return specialties;
    }

    @Override
    public Specialty findById(Long aLong) {
        Optional<Specialty> specialty = specialtyRepo.findById(aLong);
        return specialty.orElse(null);
    }

    @Override
    public Specialty save(Specialty object) {
        return specialtyRepo.save(object);
    }

    @Override
    public void deleteById(Long aLong) {
        specialtyRepo.deleteById(aLong);
    }

    @Override
    public void delete(Specialty object) {
        specialtyRepo.delete(object);
    }
}
