package spring.petclinic.services.SpringDataJpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import spring.petclinic.domain.Vet;
import spring.petclinic.repositories.VetRepo;
import spring.petclinic.services.VetService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VetJPAService implements VetService{

    private final VetRepo vetRepo;

    public VetJPAService(VetRepo vetRepo) {
        this.vetRepo = vetRepo;
    }

    @Override
    public Set<Vet> findAll() {
        Set<Vet> vets = new HashSet<>();
        vetRepo.findAll().forEach(vets::add);
        return vets;
    }

    @Override
    public Vet findById(Long aLong) {
        Optional<Vet> vet = vetRepo.findById(aLong);
        return vet.orElse(null);
    }

    @Override
    public Vet save(Vet object) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public void delete(Vet object) {

    }
}
