package spring.petclinic.services.SpringDataJpa;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import spring.petclinic.domain.Visit;
import spring.petclinic.repositories.VisitRepo;
import spring.petclinic.services.VisitService;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Profile("springdatajpa")
public class VisitJpaService implements VisitService {

    private final VisitRepo visitRepo;

    public VisitJpaService(VisitRepo visitRepo) {
        this.visitRepo = visitRepo;
    }

    @Override
    public Set<Visit> findAll() {
        Set<Visit> visits = new HashSet<>();
        visitRepo.findAll().forEach(visits::add);
        return visits;
    }

    @Override
    public Visit findById(Long aLong) {
        Optional<Visit> visit = visitRepo.findById(aLong);
        return visit.orElse(null);
    }

    @Override
    public Visit save(Visit object) {
        return visitRepo.save(object);
    }

    @Override
    public void deleteById(Long aLong) {
        visitRepo.deleteById(aLong);
    }

    @Override
    public void delete(Visit object) {
        visitRepo.delete(object);
    }
}
