package spring.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import spring.petclinic.domain.Owner;

import java.util.List;

public interface OwnerRepo extends CrudRepository<Owner,Long> {
    Owner findByLastName(String lastName);
    List<Owner> findAllByLastNameLike(String lastName);
}
