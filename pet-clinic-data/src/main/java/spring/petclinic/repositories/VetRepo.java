package spring.petclinic.repositories;


import org.springframework.data.repository.CrudRepository;
import spring.petclinic.domain.Vet;

public interface VetRepo extends CrudRepository<Vet, Long> {
}
