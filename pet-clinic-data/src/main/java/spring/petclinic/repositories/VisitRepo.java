package spring.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import spring.petclinic.domain.Visit;

public interface VisitRepo extends CrudRepository<Visit, Long> {
}
