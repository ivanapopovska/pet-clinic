package spring.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import spring.petclinic.domain.Pet;

public interface PetRepo extends CrudRepository<Pet, Long> {
}
