package spring.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;
import spring.petclinic.domain.PetType;

public interface PetTypeRepo extends CrudRepository<PetType, Long> {
}
