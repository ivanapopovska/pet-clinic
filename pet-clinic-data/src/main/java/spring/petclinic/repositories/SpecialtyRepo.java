package spring.petclinic.repositories;

import org.springframework.data.repository.CrudRepository;

public interface SpecialtyRepo extends CrudRepository<spring.petclinic.domain.Specialty, Long> {
}
